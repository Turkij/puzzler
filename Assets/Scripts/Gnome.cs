﻿using UnityEngine;
using System.Collections.Generic;

public class Gnome : Entity {

    public bool facingRight;
    public List<Instruction> instructions;
    public int currentInstruction;
    public Crate heldCrate;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        currentInstruction = 0;
    }

    // Update is called once per frame
    protected override void Update ()
    {
        base.Update();
	}

    public override int GetActionPriority()
    {
        //check if the gnome will fall
        if (IsFalling() )
        {
            return 6;
        }
        else
        {
            if (instructions != null && instructions.Count > 0)
            {
                return instructions[currentInstruction].priority;
            }
            else
            {
                return 0;
            }
        }
    }

    public override void Tick()
    {
        base.Tick();
        //check if gnome is falling
        if (IsFalling() )
        {
            if (heldCrate != null)
            {
                if (heldCrate.tile.GetTileRelative(0, -1).IsPassable())
                {
                    heldCrate.PushDown();
                }
                else
                {
                    heldCrate.isHeld = false;
                    heldCrate = null;
                }
            }
            PushDown();
        }
        else
        {
            //if not falling, do action
            if (instructions != null && instructions.Count > 0)
            {
                instructions[currentInstruction].Execute(this);
                currentInstruction %= instructions.Count;
            }
        }
        UpdateSprite();
    }

    public bool Turn()
    {
        if(heldCrate != null)
        {
            int moveDir = facingRight ? -2 : 2;
            if (!heldCrate.Move(moveDir, 0))
            {
                return false;
            }
            heldCrate.UpdateSprite();
        }
        facingRight = !facingRight;
        UpdateSprite();
        return true;
    }

    public override void UpdateSprite()
    {
        base.UpdateSprite();
        Vector3 scale = GetComponent<Transform>().localScale;
        if (facingRight != scale.x < 0)
        {
            GetComponent<Transform>().localScale = new Vector3 (-scale.x, 1, 1);
        }
    }

}
