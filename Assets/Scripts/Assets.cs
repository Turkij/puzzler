﻿using UnityEngine;
using System.Collections;

public class Assets : MonoBehaviour {

    public GameObject emptyTemplate;
    public GameObject gnomeTemplate;
    public GameObject crateTemplate;
    public GameObject crateMakerTemplate;
    public GameObject crateEaterTemplate;
    public GameObject wallTemplate;
    public GameObject InstrucitonEntryTemplate;
    public GameObject InstructionMenuTemplate;
    public GameObject lineRendererTemplate;
    public GameObject startLevelButtonTemplate;
    public Sprite startButtonSprite;
    public Sprite num1;
    public Sprite num2;
    public Sprite num3;
    public Sprite num4;
    public Sprite num5;
    public Sprite num6;
    public Sprite num7;
    public Sprite num8;
    public Sprite num9;
    public Sprite num0;
    public Sprite defaultWallSprite;
    public Sprite InstructionBlank;
    public Sprite InstructionGrab;
    public Sprite InstructionDrop;
    public Sprite InstructionTurn;
    public Sprite InstructionMoveLeft;
    public Sprite InstructionMoveRight;
    public Sprite InstructionWait;

    public Sprite getNumSprite(int num)
    {
        switch (num)
        {
            case 0:
                return num0;
            case 1:
                return num1;
            case 2:
                return num2;
            case 3:
                return num3;
            case 4:
                return num4;
            case 5:
                return num5;
            case 6:
                return num6;
            case 7:
                return num7;
            case 8:
                return num8;
            case 9:
                return num9;
            default:
                return null;
        }

    }

    void Awake()
    {
        GameController.Assets = this;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
