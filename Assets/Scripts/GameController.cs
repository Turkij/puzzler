﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

    public static GameController gameController;
    public static Assets Assets;

    public Map currentMap;
    public GameObject startLevelButton;
    public GameObject[] instructionEditors;

    public bool started;
    public int updatesToNextTick;
    public const int UPDATES_PER_TICK = 50;

    public int currentLevel;

    void Awake() {
        gameController = this;
    }

    // Use this for initialization
    void Start()
    {
        currentLevel = 1;
        BuildLevel(currentLevel);
    }

    void BuildLevel(int level)
    {
        if (currentMap != null) {
            currentMap.DestroyAll();
            currentMap = null;
        }
        bool end = false;
        switch (level)
        {
            case 1:
                currentMap = makeMap1();
                break;
            case 2:
                currentMap = makeMap2();
                break;
            case 3:
                currentMap = makeMap3();
                break;
            case 4:
                currentMap = makeMap4();
                break;
            default:
                Application.LoadLevel("Victory");
                end = true;
                break;
        }
        if(!end){
            Camera.main.GetComponent<Transform>().position = new Vector3(currentMap.width / 2f, currentMap.height / 2f, -10);
            startLevelButton = Instantiate(Assets.startLevelButtonTemplate);
            startLevelButton.GetComponent<Transform>().position = Camera.main.GetComponent<Transform>().position + new Vector3(0, 4.5f, 10);
            updatesToNextTick = UPDATES_PER_TICK;
            started = false;
        }
    }

	// Update is called once per frame
	void Update () {
        if (started)
        {
            if (updatesToNextTick <= 0)
            {
                currentMap.TickAll();
                updatesToNextTick = UPDATES_PER_TICK;
            }
            else
            {
                --updatesToNextTick;
            }
        }
        if(currentMap != null && currentMap.IsWon())
        {
            BuildLevel(++currentLevel);
        }
    }

    public void StartLevel()
    {
        started = true;
        Destroy(startLevelButton);
        foreach(GameObject editor in instructionEditors)
        {
            List<Instruction> instructions = editor.GetComponent<InstructionMenu>().GetInstructions();
            foreach(GameObject gnome in editor.GetComponent<InstructionMenu>().gnomes)
            {
                gnome.GetComponent<Gnome>().instructions = instructions;
            }
            editor.GetComponent<InstructionMenu>().DestroyLines();
            Destroy(editor);
        }
        instructionEditors = null;
    }

    //maps
    Map makeMap1()
    {
        bool[,] passableMap = { {false, false, false},
                                {false, true , false},
                                {false, true , false},
                                {false, true , false},
                                {false, true , false},
                                {false, true , false},
                                {false, true , true },
                                {false, true , false},
                                {false, false, false}};

        instructionEditors = new GameObject[1];
        Map map = new Map(passableMap, Assets.defaultWallSprite);
        Gnome g = map.addEntity<Gnome>(7,1);
        instructionEditors[0] = Instantiate(GameController.Assets.InstructionMenuTemplate);
        instructionEditors[0].GetComponent<InstructionMenu>().AttachGnome(g.gameObject );
        instructionEditors[0].GetComponent<Transform>().position = new Vector3(10f, 3, 0);
        CrateMaker cm = map.addEntity<CrateMaker>(6, 2);
        CrateEater ce = map.addEntity<CrateEater>(1, 1);
        ce.numCrates = 1;
        cm.numCrates = 1;
        g.facingRight = false;
        return map;
    }

    Map makeMap2()
    {
        bool[,] passableMap = { {false, false, false, false, false},
                                {false, false, false, false, false},
                                {false, true, false, false, false},
                                {false, true , false, false, false},
                                {false, true , true , true , false},
                                {false, true , true , true , false},
                                {false, false, false, true , false},
                                {false, false, false, true , true },
                                {false, false, false, false, false}};

        instructionEditors = new GameObject[1];
        Map map = new Map(passableMap, Assets.defaultWallSprite);
        instructionEditors[0] = Instantiate(GameController.Assets.InstructionMenuTemplate);
        Gnome g = map.addEntity<Gnome>(6, 3);
        g.facingRight = true;
        g.UpdateSprite();
        instructionEditors[0].GetComponent<InstructionMenu>().AttachGnome(g.gameObject);
        instructionEditors[0].GetComponent<Transform>().position = new Vector3(10f, 3, 0);
        CrateMaker cm = map.addEntity<CrateMaker>(7, 4);
        CrateEater ce = map.addEntity<CrateEater>(2, 1);
        ce.numCrates = 1;
        cm.numCrates = 1;
        return map;
    }

    Map makeMap3()
    {
        bool[,] passableMap = { {false, false, false, false, false},
                                {false, false, false, false, false},
                                {false, true, false, false, false},
                                {false, true , false, false, false},
                                {false, true , false, false, false},
                                {false, true , true , true , false},
                                {false, false, false, true , false},
                                {false, false, false, true , true },
                                {false, false, false, false, false}};

        instructionEditors = new GameObject[2];
        Map map = new Map(passableMap, Assets.defaultWallSprite);
        instructionEditors[0] = Instantiate(GameController.Assets.InstructionMenuTemplate);
        instructionEditors[1] = Instantiate(GameController.Assets.InstructionMenuTemplate);
        Gnome g = map.addEntity<Gnome>(4, 1);
        g.facingRight = true;
        g.UpdateSprite();
        instructionEditors[0].GetComponent<InstructionMenu>().AttachGnome(g.gameObject);
        instructionEditors[0].GetComponent<Transform>().position = new Vector3(-1f, 3, 0);
        Gnome g2 = map.addEntity<Gnome>(6, 3);
        g2.facingRight = false;
        g2.UpdateSprite();
        instructionEditors[1].GetComponent<InstructionMenu>().AttachGnome(g2.gameObject);
        instructionEditors[1].GetComponent<Transform>().position = new Vector3(10f, 3, 0);
        CrateMaker cm = map.addEntity<CrateMaker>(7, 4);
        CrateEater ce = map.addEntity<CrateEater>(2, 1);
        ce.numCrates = 2;
        cm.numCrates = 2;
        return map;
    }

    Map makeMap4()
    {
        bool[,] passableMap = { {false, false, false, false, false, false, false},
                                {false, true , true , true , false, false, false},
                                {false, false, false, true , false, false, false},
                                {false, false, false, true , true , true , false},
                                {false, false, false, false, false, true , false},
                                {false, false, false, false, false, true , true },
                                {false, false, false, false, false, false, false},
                                {false, false, false, false, false, false, false},
                                {false, false, false, false, false, false, false}};

        instructionEditors = new GameObject[1];
        Map map = new Map(passableMap, Assets.defaultWallSprite);
        instructionEditors[0] = Instantiate(GameController.Assets.InstructionMenuTemplate);
        Gnome g = map.addEntity<Gnome>(2, 3);
        g.facingRight = true;
        g.UpdateSprite();
        Gnome g2 = map.addEntity<Gnome>(4, 5);
        g2.facingRight = true;
        g2.UpdateSprite();
        instructionEditors[0].GetComponent<InstructionMenu>().AttachGnome(g.gameObject);
        instructionEditors[0].GetComponent<InstructionMenu>().AttachGnome(g2.gameObject);
        instructionEditors[0].GetComponent<Transform>().position = new Vector3(10f, 3, 0);
        CrateMaker cm = map.addEntity<CrateMaker>(5, 6);
        CrateEater ce = map.addEntity<CrateEater>(1, 1);
        ce.numCrates = 4;
        cm.numCrates = 4;
        return map;
    }
}
