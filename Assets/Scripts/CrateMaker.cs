﻿using UnityEngine;
using System.Collections;

public class CrateMaker : Entity {

    public int turnsToSpawn;
    public int resetTurns;
    public int numCrates;
    GameObject num1;
    GameObject num2;

    void Awake()
    {
        num1 = Instantiate(GameController.Assets.emptyTemplate);
        num2 = Instantiate(GameController.Assets.emptyTemplate);
    }
    // Use this for initialization
    protected override void Start () {
        base.Start();
        turnsToSpawn = 2;
        resetTurns = 2;
        UpdateSprite();
    }

    public override void Tick()
    {
        base.Tick();
        --turnsToSpawn;
        if (turnsToSpawn == 0 && numCrates > 0)
        {
            if(tile.map.addEntity<Crate>(tile.x, tile.y) != null)
            {
                --numCrates;
            }
            turnsToSpawn = resetTurns;
        }

        UpdateSprite();
    }

    public override void UpdateSprite()
    {
        base.UpdateSprite();
        int n1 = numCrates / 10;
        int n2 = numCrates % 10;
        num1.GetComponent<SpriteRenderer>().sprite = GameController.Assets.getNumSprite(n1);
        num2.GetComponent<SpriteRenderer>().sprite = GameController.Assets.getNumSprite(n2);
        num1.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayer.NameToID("Tiles4");
        num2.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayer.NameToID("Tiles4");
        num1.GetComponent<Transform>().position = new Vector3(tile.x - 0.12f, tile.y + 0.08f, 0);
        num2.GetComponent<Transform>().position = new Vector3(tile.x + 0.12f, tile.y + 0.08f, 0);

    }

    // Update is called once per frame
    protected override void Update () {
        base.Update();
	}

    void OnDestroy()
    {
        Destroy(num1.gameObject);
        Destroy(num2.gameObject);
    }

    public override int GetActionPriority()
    {
        return 0;
    }
}
