﻿using UnityEngine;
using System.Collections;

public class CrateEater : Entity {

    public int numCrates;
    public int turnsToDestroy;
    public int maxTurnsToDestroy;
    GameObject num1;
    GameObject num2;

    void Awake()
    {
        num1 = Instantiate(GameController.Assets.emptyTemplate);
        num2 = Instantiate(GameController.Assets.emptyTemplate);
    }

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        maxTurnsToDestroy = 1;
        turnsToDestroy = maxTurnsToDestroy;
        UpdateSprite();
    }
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void Tick()
    {
        base.Tick();
        if(tile.entity != null && tile.entity.GetType() == typeof(Crate) && !(tile.entity as Crate).isHeld)
        {
            if (turnsToDestroy == 0 && numCrates > 0)
            {
                tile.map.RemoveEntity(tile.entity);
                tile.entity = null;
                --numCrates;
            }
            else
            {
                --turnsToDestroy;
            }
        }
        else
        {
            turnsToDestroy = maxTurnsToDestroy;
        }
        UpdateSprite();
    }

    public override void UpdateSprite()
    {
        base.UpdateSprite();
        int n1 = numCrates / 10;
        int n2 = numCrates % 10;
        num1.GetComponent<SpriteRenderer>().sprite = GameController.Assets.getNumSprite(n1);
        num2.GetComponent<SpriteRenderer>().sprite = GameController.Assets.getNumSprite(n2);
        num1.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayer.NameToID("Tiles2");
        num2.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayer.NameToID("Tiles2");
        num1.GetComponent<Transform>().position = new Vector3(tile.x - 0.25f, tile.y + 0.28f, 0);
        num2.GetComponent<Transform>().position = new Vector3(tile.x + -0.03f, tile.y + 0.28f, 0);

    }

    public override int GetActionPriority()
    {
        return 0;
    }

    void OnDestroy()
    {
        Destroy(num1.gameObject);
        Destroy(num2.gameObject);
    }
}
