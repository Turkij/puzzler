﻿using UnityEngine;
using System.Collections;

public abstract class Instruction
{

    public int priority;//priority: fall > turn > moveLeft > moveRight > drop > grab > wait
    public Instruction(int priority)
    {
        this.priority = priority;
    }
    public abstract void Execute(Gnome target);

    public class MoveLeft : Instruction
    {
        public MoveLeft() : base(4)
        {

        }
        public override void Execute(Gnome target)
        {
            if (target.facingRight)
            {
                if (!target.Turn())
                {
                    return;
                }
            }
            if (target.heldCrate != null)
            {
                target.heldCrate.PushLeft();
            }
            target.PushLeft();
            ++target.currentInstruction;
        }
    }
    
    public class MoveRight : Instruction
    {
        public MoveRight() : base(3)
        {
        }
        public override void Execute(Gnome target)
        {
            if (!target.facingRight)
            {
                if (!target.Turn())
                {
                    return;
                }
            }
            if (target.heldCrate != null)
            {
                target.heldCrate.PushRight();
            }
            target.PushRight();
            ++target.currentInstruction;
        }
    }

    public class Turn : Instruction
    {
        public Turn() : base(5)
        {

        }
        public override void Execute(Gnome target)
        {
            target.Turn();
            ++target.currentInstruction;
        }
    }

    public class Grab : Instruction
    {
        public Grab() : base(1)
        {

        }
        public override void Execute(Gnome target)
        {
            int x = target.facingRight ? 1 : -1;
            Crate grabCandidate = target.tile.GetTileRelative(x, 0).entity as Crate;
            if (grabCandidate != null && !grabCandidate.isHeld)
            {
                grabCandidate.isHeld = true;
                target.heldCrate = grabCandidate;
                grabCandidate.UpdateSprite();
            }
            ++target.currentInstruction;
        }
    }

    public class Wait : Instruction
    {
        public Wait() : base(0)
        {

        }
        public override void Execute(Gnome target)
        {
            ++target.currentInstruction;
        }
    }

    public class Drop : Instruction
    {
        public Drop() : base(2)
        {

        }
        public override void Execute(Gnome target)
        {
            if (target.heldCrate != null)
            {
                target.heldCrate.isHeld = false;
                target.heldCrate.UpdateSprite();
                target.heldCrate = null;
            }
            ++target.currentInstruction;
        }
    }
}