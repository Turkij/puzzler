﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InstructionMenu : MonoBehaviour {

    public List<GameObject> gnomes;
    public List<GameObject> lines;
    public List<GameObject> entries;
    public Vector3 lastMousePos;
    public bool dragging;
    public const int MAX_ENTRIES = 20;

    void Awake()
    {
        GetComponent<Transform>().position = new Vector3(0, 0, 0);
        gnomes = new List<GameObject>();
        entries = new List<GameObject>();
        AddInstructionEntry();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        for(int i = 0;i< gnomes.Count;++i)
        {
            LineRenderer renderer = lines[i].GetComponent<LineRenderer>();
            renderer.SetPosition(0, GetComponent<Transform>().position + new Vector3(1.9f, 2.2f, 0));
            renderer.SetPosition(1, gnomes[i].GetComponent<Transform>().position + new Vector3(0, 0, 0));
        }
	}

    void OnMouseDown()
    {
        dragging = true;
        lastMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - GetComponent<Transform>().position;
    }

    void OnMouseUp()
    {
        dragging = false;
    }

    void OnMouseDrag()
    {
        if (dragging)
        {
            GetComponent<Transform>().position = Camera.main.ScreenToWorldPoint(Input.mousePosition) - lastMousePos;
        }
    }

    public void AttachGnome(GameObject g)
    {
        gnomes.Add(g);
        GameObject newLine = Instantiate(GameController.Assets.lineRendererTemplate);
        LineRenderer renderer = newLine.GetComponent<LineRenderer>();
        renderer.sortingLayerName = "Menu1";
        lines.Add(newLine);
    }

    public void DetachGnome(GameObject g)
    {
        int index = gnomes.IndexOf(g);
        if(index != -1)
        {
            gnomes.RemoveAt(index);
            Destroy(lines[index]);
            lines.RemoveAt(index);
        }
    }

    public bool AddInstructionEntry()
    {
        return AddInstructionEntry(entries.Count);
    }

    public bool AddInstructionEntry(int index)
    {
        if (entries.Count < MAX_ENTRIES)
        {
            GameObject InstructionEntryObject = Instantiate(GameController.Assets.InstrucitonEntryTemplate);
            InstructionEntryObject.GetComponent<Transform>().position = GetComponent<Transform>().position + new Vector3(0, 1.7f - 0.5f * index, 0);
            InstructionEntryObject.GetComponent<Transform>().parent = GetComponent<Transform>();
            InstructionEntryObject.GetComponent<InstructionEntry>().parent = this;
            InstructionEntryObject.GetComponent<InstructionEntry>().thisIndex = index;
            entries.Insert(index, InstructionEntryObject);
            for (int i = index + 1; i < entries.Count; ++i)
            {
                entries[i].GetComponent<InstructionEntry>().thisIndex += 1;
                UpdateEntrySprite(i);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public void RemoveInstructionEntry(int index)
    {
        GameObject toRemove = entries[index];
        entries.RemoveAt(index);
        for (int i= index; i < entries.Count; ++i)
        {
            entries[i].GetComponent<InstructionEntry>().thisIndex -= 1;
            UpdateEntrySprite(i);
        }
        Destroy(toRemove);
        if(entries.Count == 0)
        {
            AddInstructionEntry();
        }
    }

    public void UpdateEntrySprite(int index)
    {
        entries[index].GetComponent<Transform>().position = GetComponent<Transform>().position + new Vector3(0, 1.7f - 0.5f * index, 0);
    }

    public List<Instruction> GetInstructions()
    {
        List<Instruction> result = new List<Instruction>();
        foreach (GameObject entry in entries)
        {
            Instruction inst = entry.GetComponent<InstructionEntry>().GetInstruction();
            if(inst != null)
            {
                result.Add(inst);
            }
        }
        return result;
    }

    public void DestroyLines()
    {
        foreach(GameObject line in lines)
        {
            Destroy(line);
        }
    }
}
