﻿using UnityEngine;
using System.Collections;

public abstract class Entity : MonoBehaviour {
    
    public Tile tile;
    public float[] positionIncrement;
    public int ticksToSlide;
    public Vector3 originalPos;
    
    void Awake()
    {
        positionIncrement = new float[2];
        positionIncrement[0] = 0;
        positionIncrement[1] = 0;
        ticksToSlide = 0;
    }
	// Use this for initialization
	protected virtual void Start () {
        
    }

    // Update is called once per frame
    protected virtual void Update ()
    {
        if (ticksToSlide > 0)
        {
            GetComponent<Transform>().position = originalPos + new Vector3((GameController.UPDATES_PER_TICK - ticksToSlide)*positionIncrement[0], (GameController.UPDATES_PER_TICK - ticksToSlide) * positionIncrement[1], 0);
            --ticksToSlide;
        }
    }

    public abstract int GetActionPriority();

    public virtual void Tick()
    {
        UpdateSprite();
    }

    public virtual void UpdateSprite()
    {
        if (ticksToSlide == 0)
        {
            GetComponent<Transform>().position = new Vector3(tile.x, tile.y, 0);
        }
    }

    public virtual bool IsFalling()
    {
        Tile t = tile.GetTileRelative(0, -1);
        return t != null && t.IsPassable();
    }

    public bool Move(int xdist, int ydist)
    {
        return MoveTo(tile.x + xdist, tile.y + ydist);
    }

    public bool MoveTo(int x, int y)
    {
        Tile t = tile;
        Map map = t.map;
        if (x >= 0 && x <= map.width - 1 && y >= 0 && y <= map.height - 1)
        {
            Tile t2 = map.tiles[x, y];
            if (t2.IsPassable())
            {
                t2.entity = this;
                this.tile = t2;
                t.entity = null;
                return true;
            }
            return false;
        }
        return false;
    }

    public bool SmoothMove(int xdist, int ydist)
    {
        int x = tile.x + xdist;
        int y = tile.y + ydist;
        Tile t = tile;
        Map map = t.map;
        if (x >= 0 && x <= map.width - 1 && y >= 0 && y <= map.height - 1)
        {
            Tile t2 = map.tiles[x, y];
            if (t2.IsPassable())
            {
                t2.entity = this;
                this.tile = t2;
                t.entity = null;
                positionIncrement[0] = (float)xdist / GameController.UPDATES_PER_TICK;
                positionIncrement[1] = (float)ydist / GameController.UPDATES_PER_TICK;
                originalPos = GetComponent<Transform>().position;
                ticksToSlide = GameController.UPDATES_PER_TICK;
                return true;
            }
            return false;
        }
        return false;
    }

    public bool PushLeft()
    {
        return SmoothMove(-1, 0);
    }

    public bool PushRight()
    {
        return SmoothMove(1, 0);
    }

    public bool PushUp()
    {
        return SmoothMove(0, 1);
    }

    public bool PushDown()
    {
        return SmoothMove(0, -1);
    }
}
