﻿using UnityEngine;
using System.Collections.Generic;

public class Tile : MonoBehaviour{

    public Entity entity;
    public Map map;
    public int x, y;
    public Sprite sprite;
    private bool isPassable;

    public static Tile prefab;

    public void init(Map map, int x, int y, bool isPassable, Sprite sprite)
    {
        this.map = map;
        this.x = x;
        this.y = y;
        this.isPassable = isPassable;
        this.GetComponent<Transform>().position = new Vector3(x, y, 0);
        if (sprite != null)
        {
            GetComponent<SpriteRenderer>().sprite = sprite;
        }
    }

    public bool IsPassable() {
        return isPassable && entity == null;
    }

    public Tile GetTileRelative(int x, int y)
    {
        if (this.x + x >= 0 && this.x + x <= map.width - 1 && this.y + y >= 0 && this.y + y <= map.height - 1)
        {
            return map.tiles[this.x + x, this.y + y];
        }
        return null;
    }

}
