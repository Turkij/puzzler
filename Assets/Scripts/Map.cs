﻿using System.Collections.Generic;
using UnityEngine;

public class Map {

    public int width, height;
    public Tile[,] tiles;
    public List<Entity> entities;
    public List<CrateEater> crateEaters;

	public Map(int width, int height) {
        entities = new List<Entity>();
        tiles = new Tile[width, height];
        this.width = width;
        this.height = height;
        for(int x = 0; x < width; ++x)
        {
            for (int y = 0; y < height; ++y)
            {
                setTile(x, y, true, null);
            }

        }
    }

    public Map(bool[,] passableMap, Sprite sprite)
    {
        entities = new List<Entity>();
        crateEaters = new List<CrateEater>();
        this.width = passableMap.GetLength(0);
        this.height = passableMap.GetLength(1);
        tiles = new Tile[width, height];
        for (int x = 0; x < width; ++x)
        {
            for (int y = 0; y < height; ++y)
            {
                if (passableMap[x, y])
                {
                    setTile(x, y, true, null);
                }
                else
                {
                    setTile(x, y, false, sprite);
                }
            }

        }
    }

    public Map(bool[,] passableMap, Sprite[,] spriteMap) {
        entities = new List<Entity>();
        this.width = passableMap.GetLength(1);
        this.height = passableMap.GetLength(0);
        tiles = new Tile[width, height];
        for (int x = 0; x < width; ++x)
        {
            for (int y = 0; y < height; ++y)
            {
                setTile(y, x, passableMap[x,y], spriteMap[x,y]);
            }

        }
    }

    public Tile setTile(int x, int y, bool isPassable, Sprite sprite)
    {
        Tile t = UnityEngine.Object.Instantiate(GameController.Assets.wallTemplate).GetComponent<Tile>();
        t.init(this, x, y, isPassable, sprite);
        if(tiles[x, y] != null){
            UnityEngine.Object.Destroy(tiles[x, y]);
        }
        tiles[x, y] = t;
        return t;
    }

    public T addEntity<T> (int x, int y) where T : Entity
    {
        if (tiles[x, y].IsPassable() )
        {
            if (typeof(T) == typeof(Gnome))
            {
                if (tiles[x, y].entity == null)
                {
                    Entity e = UnityEngine.Object.Instantiate(GameController.Assets.gnomeTemplate).GetComponent<Gnome>();
                    entities.Add(e);
                    e.tile = tiles[x, y];
                    tiles[x, y].entity = e;
                    e.UpdateSprite();
                    return (T)e;
                }
            }
            else if (typeof(T) == typeof(Crate))
            {

                if (tiles[x, y].entity == null)
                {
                    Entity e = UnityEngine.Object.Instantiate(GameController.Assets.crateTemplate).GetComponent<Crate>();
                    entities.Add(e);
                    e.tile = tiles[x, y];
                    tiles[x, y].entity = e;
                    e.UpdateSprite();
                    return (T)e;
                }
            }
            else if (typeof(T) == typeof(CrateMaker))
            {
                if (tiles[x, y].entity == null) {
                    Entity e = UnityEngine.Object.Instantiate(GameController.Assets.crateMakerTemplate).GetComponent<CrateMaker>();
                    entities.Add(e);
                    e.tile = tiles[x, y];
                    e.UpdateSprite();
                    return (T)e;
                }
            }
            else if (typeof(T) == typeof(CrateEater))
            {
                if (tiles[x, y].entity == null)
                {
                    Entity e = UnityEngine.Object.Instantiate(GameController.Assets.crateEaterTemplate).GetComponent<CrateEater>();
                    entities.Add(e);
                    e.tile = tiles[x, y];
                    e.UpdateSprite();
                    crateEaters.Add(e.GetComponent<CrateEater>());
                    return (T)e;
                }
            }
            else
            {
                throw new System.Exception("There is no add method for entity type " + typeof(T));
            }
        }
        return null;
    }

    public void TickAll()
    {
        Entity[] tickQueue = new Entity[entities.Count];
        entities.CopyTo(tickQueue);
        System.Array.Sort(tickQueue, CompareEntitiesByActionPriority);
        foreach(Entity e in tickQueue)
        {
            e.Tick();
        }
    }

    private static int CompareEntitiesByActionPriority(Entity e1, Entity e2)
    {
        return e2.GetActionPriority() - e1.GetActionPriority();//entities with higher action priority are smaller than entities with lower action priority
    }

    public bool AddEntity(Entity e, int x, int y)
    {
        if (x > 0 && x < width - 1 && y > 0 && y < height - 1)
        {
            return false;
        }
        if (!tiles[x, y].IsPassable())
        {
            return false;
        }
        tiles[x, y].entity = e;
        entities.Add(e);
        return true;
    }

    public bool RemoveEntity(Entity e)
    {
        int index = entities.IndexOf(e);
        if (index == -1)
        {
            return false;
        }
        else
        {
            entities.RemoveAt(index);
            Object.Destroy(e.tile.entity.gameObject);
            e.tile.entity = null;
            return true;
        }
    }

    public bool IsWon()
    {
        foreach(CrateEater eater in crateEaters)
        {
            if(eater.numCrates > 0)
            {
                return false;
            }
        }
        return true;
    }

    public void DestroyAll()
    {
        foreach(Tile t in tiles)
        {
            UnityEngine.Object.Destroy(t.gameObject);
        }
        foreach (Entity e in entities)
        {
            UnityEngine.Object.Destroy(e.gameObject);
        }
    }
}
