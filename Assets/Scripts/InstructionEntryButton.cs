﻿using UnityEngine;
using System.Collections;

public class InstructionEntryButton : MonoBehaviour {

    public InstructionEntry parent;
    public int buttonID;

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseUp()
    {
        parent.SendClick(buttonID);
    }
}
