﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InstructionEntry : MonoBehaviour {

    public static Sprite[] instructionNameSprites;
    public int instructionIndex;

    public GameObject upArrowButton;
    public GameObject downArrowButton;
    public GameObject xButton;
    public GameObject copyDownButton;
    public GameObject instructionName;

    public InstructionMenu parent;
    public int thisIndex;

    public void SendClick(int buttonID)
    {
        //Debug.Log(parent.entries.Count);
        //Debug.Log(thisIndex);
        //Debug.Log("Click" + thisIndex);
        switch (buttonID)
        {
            case 0:
                //instruciton name
                ++instructionIndex;
                instructionIndex %= instructionNameSprites.Length;
                instructionName.GetComponent<SpriteRenderer>().sprite = instructionNameSprites[instructionIndex];
                break;
            case 1:
                //down arrow
                if(thisIndex < parent.entries.Count - 1)
                {
                    GameObject tmp = parent.entries[thisIndex + 1];
                    parent.entries[thisIndex + 1] = parent.entries[thisIndex];
                    parent.entries[thisIndex] = tmp;
                    thisIndex++;
                    tmp.GetComponent<InstructionEntry>().thisIndex--;
                    parent.UpdateEntrySprite(thisIndex);
                    parent.UpdateEntrySprite(thisIndex - 1);
                }
                break;
            case 2:
                //up arrow
                if (thisIndex > 0)
                {
                    GameObject tmp = parent.entries[thisIndex - 1];
                    parent.entries[thisIndex - 1] = parent.entries[thisIndex];
                    parent.entries[thisIndex] = tmp;
                    thisIndex--;
                    tmp.GetComponent<InstructionEntry>().thisIndex++;
                    parent.UpdateEntrySprite(thisIndex);
                    parent.UpdateEntrySprite(thisIndex + 1);
                }
                break;
            case 3:
                //copy down
                if (parent.AddInstructionEntry(thisIndex + 1))
                {
                    InstructionEntry newEntry = parent.entries[thisIndex + 1].GetComponent<InstructionEntry>();
                    newEntry.instructionIndex = this.instructionIndex;
                    if (instructionIndex != -1)
                    {
                        newEntry.instructionName.GetComponent<SpriteRenderer>().sprite = instructionNameSprites[instructionIndex];
                    }
                }
                break;
            case 4:
                //x
                parent.RemoveInstructionEntry(thisIndex);
                break;
        }
    }

    void Awake()
    {
        if (instructionNameSprites == null)
        {
            instructionNameSprites = new Sprite[6];
            instructionNameSprites[0] = GameController.Assets.InstructionMoveLeft;
            instructionNameSprites[1] = GameController.Assets.InstructionMoveRight;
            instructionNameSprites[2] = GameController.Assets.InstructionTurn;
            instructionNameSprites[3] = GameController.Assets.InstructionGrab;
            instructionNameSprites[4] = GameController.Assets.InstructionDrop;
            instructionNameSprites[5] = GameController.Assets.InstructionWait;
        }
        instructionIndex = -1;

        upArrowButton.GetComponent<InstructionEntryButton>().parent = this;
        downArrowButton.GetComponent<InstructionEntryButton>().parent = this;
        copyDownButton.GetComponent<InstructionEntryButton>().parent = this;
        xButton.GetComponent<InstructionEntryButton>().parent = this;
        instructionName.GetComponent<InstructionEntryButton>().parent = this;
    }

    public Instruction GetInstruction()
    {
        switch (instructionIndex)
        {
            case -1:
                return null;
                break;
            case 0:
                return new Instruction.MoveLeft();
                break;
            case 1:
                return new Instruction.MoveRight();
                break;
            case 2:
                return new Instruction.Turn();
                break;
            case 3:
                return new Instruction.Grab();
                break;
            case 4:
                return new Instruction.Drop();
                break;
            case 5:
                return new Instruction.Wait();
                break;
            default:
                return null;
                break;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
