﻿using UnityEngine;
using System.Collections;
using System;

public class Crate : Entity {

    public bool isHeld;

    // Use this for initialization
    protected override void Start () {
        base.Start();
        isHeld = false;
	}

    // Update is called once per frame
    protected override void Update ()
    {
        base.Update();
	}

    public override int GetActionPriority()
    {
        if (IsFalling() )
        {
            return 6;
        }
        else
        {
            return 0;
        }
    }

    public override bool IsFalling()
    {
        return !isHeld && base.IsFalling();
    }

    public override void UpdateSprite()
    {
        base.UpdateSprite();
        if (!isHeld && !IsFalling() )
        {
            GetComponent<Transform>().position += new Vector3(0, -0.3f, 0);
        }
    }

    public override void Tick()
    {
        base.Tick();
        if (IsFalling() )
        {
            PushDown();
        }
        UpdateSprite();
    }
}
